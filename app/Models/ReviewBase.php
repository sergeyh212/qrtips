<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReviewBase extends Model
{
    use HasFactory;

    public $table = "reviews";

    protected $fillable = [];

    protected $guarded = [];

    public $timestamps = false;

    const S_APPROVE = 1;            //подтвержено
    const S_CONSIDERATION = 2;      //рассмотрение
    const S_CANCELED = 0;           //отменена
}
