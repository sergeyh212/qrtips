<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipsBase extends Model
{
    use HasFactory;

    public $table = "tips";

    protected $fillable = [];

    protected $guarded = [];

    public $timestamps = false;

}
