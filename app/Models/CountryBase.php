<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountryBase extends Model
{
    use HasFactory;

    public $table = "countries";

    public function user()
    {
        return $this->hasMany(UserBase::class);
    }
}
