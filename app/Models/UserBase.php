<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class UserBase extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    public $table = "users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $guarded = [];
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function cashflows()
    {
        return $this->hasMany(CashFlowBase::class);
    }

    public function tips()
    {
        return $this->hasMany(TipsBase::class);
    }

    public function payments()
    {
        return $this->hasMany(PaymentBase::class);
    }

    public function card()
    {
        return $this->hasOne(CardBase::class);
    }

    public function country()
    {
        return $this->belongsTo(CountryBase::class);
    }

    public function reviews()
    {
        return $this->hasMany(ReviewBase::class);
    }


}
