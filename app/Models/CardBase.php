<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardBase extends Model
{
    use HasFactory;

    public $table = "cards";

    protected $fillable = [];

    protected $guarded = [];

    public $timestamps = false;

}
