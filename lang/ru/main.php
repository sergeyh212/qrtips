<?php

return [
    'current_lang' => 'RU',

                    //employee
    //sign up

    'sign_up' => 'Регистрация',
    'phone' => 'Номер телефон',
    'have_account_signup' => 'Уже есть аккаунт?',
    'sign_in_here' => 'Войдите!',
    'sign_up_google' => 'Войти через Google',
    'sign_up_facebook' => 'Войти через Facebook',
    'sign_up_email' => 'Войти через Email',
    'first_name' => 'Имя',
    'last_name' => 'Фамилия',
    'country' => 'Страна',
    'email_address' => 'Электронная почта',
    'password' => 'Пароль',
    'confirm_password' => 'Подтвердите пароль',
    'enter_password' => 'Введите пароль',
    'terms' => 'Я прочитал и согласен с условиями пользования',
    'language' => 'Язык',
    'en' => 'Английский',
    'ru' => 'Русский',

    //sign in

    'sign_in' => 'Вход',
    'have_account_signin' => 'Нет аккаунта?',
    'sign_up_here' => 'Зарегистрируйтесь!',
    'sign_in_google' => 'Войти через Google',
    'sign_in_facebook' => 'Войти через Facebook',
    'sign_in_email' => 'Войти через Email',
    'forgot_password' => 'Забыли пароль?',

    // left nav
    'user_profile' => 'Профиль',
    'qr' => 'Qr-code',
    'balance' => 'Баланс',
    'reviews' => 'Отзывы',

    // profile
    'user' => 'Пользователь',
    'settings' => 'Настройки',
    'save_changes' => 'Сохранить изменения',
    'commission' => 'Коммиссия',

    // qr
    'send_a_tips' => 'Отправить чаевые',
    'send' => 'Отправить',

    // header
    'profile' => 'Профиль',
    'employee' => 'Сотрудник',
    'logout' => 'Выход',
    'search' => 'Поиск...',

    // balance
    'money' => 'Деньги',
    'paid' => 'Выведено',
    'card' => 'Карта',
    'add_card' => 'Добавить карту',
    'not_enough_money' => 'Недостаточно средств!',
    'withdraw' => 'Вывод',
    'amount' => 'Сумма',
    'currency' => 'Валюта',
    'time' => 'Время',


    // add card
    'payment_info' => 'Информация',
    'name' => 'Имя',
    'card_number' => 'Номер карты',
    'expiration' => 'Срок действия(мм/гг)',
    'security_code' => 'Код безопасности',
    'add' => 'Добавить',

    // send tips
    'tips' => 'Чаевые',
    'review_write' => 'Напишите свой отзыв',
    'review' => 'Отзыв',
    'anonym' => 'Аноним',
    'rating' => 'Оценка',


    // countries
    "Austria" => "#Austria",
    "Belgium" => "#Belgium",
    "Bulgaria" => "#Bulgaria",
    "Croatia" => "#Croatia",
    "Czech Republic" => "#Czech Republic",
    "Denmark" => "#Denmark",
    "England" => "#England",
    "Estonia" => "#Estonia",
    "Finland" => "#Finland",
    "France" => "#France",
    "Germany" => "#Germany",
    "Greece" => "#Greece",
    "Hungary" => "#Hungary",
    "Italy" => "#Italy",
    "Latvia" => "#Latvia",
    "Liechtenstein" => "#Liechtenstein",
    "Lithuania" => "#Lithuania",
    "Luxembourg" => "#Luxembourg",
    "Netherlands" => "#Netherlands",
    "Norway" => "#Norway",
    "Poland" => "#Poland",
    "Portugal" => "#Portugal",
    "Romania" => "#Romania",
    "Scotland" => "#Scotland",
    "Spain" => "#Spain",
    "Sweden" => "#Sweden",
    "Switzerland" => "#Switzerland",
    "Turkey" => "#Turkey",
    "UK" => "#UK",
    "Vatican" => "#Vatican",








            // admin

    'main_page' => '#Main Page',
    'users' => '#Users',
    'users_table' => '#Users Table',
    'show' => '#Show',
    'entries' => '#entris',
    'search:' => '#Search:',
    'reg_at' => '#Registered at',
    'upd_at' => '#Updated at',
    'actions' => '#Actions',
    'showing' => '#Showing',
    'prev' => '#Prev',
    'next' => '#Next',
    'tips_table' => '#Tips Table',
    'type' => '#Type',
    'status' => '#Status',
    'user_id' => '#User ID',
    'payments_table' => '#Payments Table',
    'reviews_table' => '#Reviews Table',
    'reviewer_name' => '#Reviewer Name',
    'payments' => '#Payments'
];
