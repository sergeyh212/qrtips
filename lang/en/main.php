<?php

return [
    'current_lang' => 'EN',

                //employee
    //sign up

    'sign_up' => 'Sign Up',
    'phone' => 'Phone',
    'have_account_signup' => 'Already have an account?',
    'sign_in_here' => 'Sign in here',
    'sign_up_google' => 'Sign Up with Google',
    'sign_up_facebook' => 'Sign Up with Facebook',
    'sign_up_email' => 'OR SIGN UP WITH EMAIL',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'country' => 'Country',
    'email_address' => 'Email Address',
    'password' => 'Password',
    'confirm_password' => 'Confirm Password',
    'enter_password' => 'Enter Password',
    'terms' => 'I read and agree to Terms & Conditions',
    'language' => 'Language',
    'en' => 'English',
    'ru' => 'Russian',

    //sign in

    'sign_in' => 'Sign In',
    'have_account_signin' => 'Don\'t have an account yet?',
    'sign_up_here' => 'Sign Up here',
    'sign_in_google' => 'Sign in with Google',
    'sign_in_facebook' => 'Sign in with Facebook',
    'sign_in_email' => 'OR SIGN IN WITH EMAIL',
    'forgot_password' => 'Forgot Password ?',

    // left nav
    'user_profile' => 'User Profile',
    'qr' => 'Qr-code',
    'balance' => 'Balance',
    'reviews' => 'Reviews',


    // profile
    'user' => 'User',
    'settings' => 'Settings',
    'save_changes' => 'Save Changes',
    'commission' => 'Commission',

    // qr
    'send_a_tips' => 'Send a tips',
    'send' => 'Send',

    // header
    'profile' => 'Profile',
    'employee' => 'Employee',
    'logout' => 'Logout',
    'search' => 'Type to search...',

    // balance
    'money' => 'Money',
    'paid' => 'Paid',
    'card' => 'Card',
    'add_card' => 'Add Card',
    'not_enough_money' => 'Not enough money!',
    'withdraw' => 'Withdraw',
    'amount' => 'Amount',
    'currency' => 'Currency',
    'time' => 'Time',


    // send tips
    'tips' => 'Tips',
    'review_write' => 'Write your review',
    'review' => 'Review',
    'anonym' => 'Anonym',
    'rating' => 'Rating',


    // add card
    'payment_info' => 'Payment Information',
    'name' => 'Name',
    'card_number' => 'Card Number',
    'expiration' => 'Expiration (mm/yy)',
    'security_code' => 'Security Code',
    'add' => 'Add',

    // countries
    "Austria" => "Austria",
    "Belgium" => "Belgium",
    "Bulgaria" => "Bulgaria",
    "Croatia" => "Croatia",
    "Czech Republic" => "Czech Republic",
    "Denmark" => "Denmark",
    "England" => "England",
    "Estonia" => "Estonia",
    "Finland" => "Finland",
    "France" => "France",
    "Germany" => "Germany",
    "Greece" => "Greece",
    "Hungary" => "Hungary",
    "Italy" => "Italy",
    "Latvia" => "Latvia",
    "Liechtenstein" => "Liechtenstein",
    "Lithuania" => "Lithuania",
    "Luxembourg" => "Luxembourg",
    "Netherlands" => "Netherlands",
    "Norway" => "Norway",
    "Poland" => "Poland",
    "Portugal" => "Portugal",
    "Romania" => "Romania",
    "Scotland" => "Scotland",
    "Spain" => "Spain",
    "Sweden" => "Sweden",
    "Switzerland" => "Switzerland",
    "Turkey" => "Turkey",
    "UK" => "UK",
    "Vatican" => "Vatican",







            // admin

    'main_page' => 'Main Page',
    'users' => 'Users',
    'users_table' => 'Users Table',
    'show' => 'Show',
    'entries' => 'entris',
    'search:' => 'Search:',
    'reg_at' => 'Registered at',
    'upd_at' => 'Updated at',
    'actions' => 'Actions',
    'showing' => 'Showing',
    'prev' => 'Prev',
    'next' => 'Next',
    'tips_table' => 'Tips Table',
    'type' => 'Type',
    'status' => 'Status',
    'user_id' => 'User ID',
    'payments_table' => 'Payments Table',
    'reviews_table' => 'Reviews Table',
    'reviewer_name' => 'Reviewer Name',
    'payments' => 'Payments'
];
