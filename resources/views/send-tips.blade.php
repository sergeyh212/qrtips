<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--favicon-->
    <link rel="icon" href="../../assets/images/favicon-32x32.png" type="image/png"/>
    <!--plugins-->
    @yield("style")
    <link href="../../public/assets" rel="stylesheet"/>
    <link href="../../assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
    <link href="../../assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet"/>
    <link href="../../assets/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet"/>
    <!-- loader-->
    <link href="../../assets/css/pace.min.css" rel="stylesheet"/>
    <script src="../../assets/js/pace.min.js"></script>
    <!-- Bootstrap CSS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link href="../../assets/css/app.css" rel="stylesheet">
    <link href="../../assets/css/icons.css" rel="stylesheet">

    <!-- Theme Style CSS -->
    <link rel="stylesheet" href="../../assets/css/dark-theme.css"/>
    <link rel="stylesheet" href="../../assets/css/semi-dark.css"/>
    <link rel="stylesheet" href="../../assets/css/header-colors.css"/>
    <title>Tips</title>
</head>


<body>
<div class="card" style="width: 40rem;margin-left: auto;margin-right: auto;">
    <div class="card-body">
        <h5 class="card-title text-center">@lang('main.send_a_tips')</h5>
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="list-unstyled">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="post" action="{{ route('tips.sendTips', ['id' => $userId])  }}">
                    @csrf
                    <label for="validationDefault01" class="form-label">@lang("main.tips")</label>
                    <div class="input-group mb-3"><span class="input-group-text">$</span>
                        <input type="number" class="form-control" name="tips_count">
                    </div>
                    <div class="mb-3">
                        <label for="validationDefault01" class="form-label">@lang("main.name")</label>
                        <input type="text" class="form-control" id="validationDefault01" name="reviewer_name" value="Anonym">
                    </div>
                    <div class="mb-3">
                        <label for="validationTextarea" class="form-label">@lang("main.review")</label>
                        <textarea class="form-control" id="validationTextarea"
                                  placeholder="@lang("main.review_write")" name="review"></textarea>
                    </div>
                    <div class="center">
                        <button type="submit" class="btn btn-primary" id="reset-btn"
                                value="Send"></i>@lang('main.send')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>





