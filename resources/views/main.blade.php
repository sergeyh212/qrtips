<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>QRTips</title>

    <link rel="stylesheet" href="assets/libs/swiper/swiper-bundle.min.css">
    <link rel="stylesheet" href="assets/libs/animate.css">
    <link rel="stylesheet" href="assets/css/main.css">

</head>

<body>

    <header class="header">
        <div class="c-container">
            <div class="header-wrapper">
                <div class="header-logo"><img src="{{ asset('assets/images/logo.png') }}" alt=""></div>
                <div class="header-contacts">
                    <div class="header-contacts-item">tel.: <a href="tel:">+333 33 333 33 33</a></div>
                    <div class="header-contacts-item header-email">e-mail: <a
                            href="mailto:qrtips@gmail.com">qrtips@gmail.com</a></div>
                </div>
            </div>
        </div>
    </header>

    <section class="main">
        <div class="main-wrapper">
            <div class="c-container">
                <div class="main-wrap">
                    <div class="main-info">
                        <h1 class="main-title fadeInUp wow">A cashless way <br>to tip staff!</h1>
                        <ul class="main-list">
                            <li><img src="{{ asset('assets/images/check.svg') }}" alt=""> Receive tips instantly
                            </li>
                            <li><img src="{{ asset('assets/images/check.svg') }}" alt=""> To the card of any
                                bank</li>
                            <li><img src="{{ asset('assets/images/check.svg') }}" alt=""> 100% transparency on
                                each</li>
                        </ul>
                    </div>
                    <div class="main-img fadeInRight wow">
                        <img src="{{ asset('assets/images/main-img.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="blocks-wrap">
        <div class="blocks-bg"></div>

        <img class="solution-shadow" src="{{ asset('assets/images/solution-shadow.png') }}" alt="">
        <img class="solution-shadow-tablet" src="{{ asset('assets/images/solution-shadow-tablet.png') }}"
            alt="">
        <img class="solution-shadow-mob" src="{{ asset('assets/images/solution-shadow-mob.png') }}" alt="">

        <section class="solution">
            <div class="c-container">
                <div class="solution-wrapper">
                    <div class="solution-img fadeInLeft wow">
                        <img src="{{ asset('assets/images/solution-img.png') }}" alt="">
                    </div>
                    <div class="solution-info">
                        <h2 class="solution-title s-title fadeInUp wow">QRTips — the best solution for cafes and
                            restaurants!</h2>
                        <p class="solution-text">Tipping in a cafe is 5-15% of the check, but can reach 20-30% if the
                            service was at a high level. But increasingly, staff are losing the opportunity to get their
                            tips because guests don’t have any cash.</p>
                        <p class="solution-text">QRTips offers an instant way for guests to tip and also exspress
                            appreciation with a rating and comments/ Simply point camera at the QR code displayed on the
                            signage or checks.</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="revenue">
            <div class="c-container">
                <h2 class="revenue-title s-title fadeInUp wow">QRTips — triple<br> your tips revenue!</h2>
                <div class="revenue-items">
                    <div class="revenue-item">
                        <div class="revenue-item-number">92%</div>
                        <p class="revenue-item-text">of guests sometimes tip*</p>
                    </div>
                    <div class="revenue-item">
                        <div class="revenue-item-text-top">waiters get</div>
                        <div class="revenue-item-number">70-90%</div>
                        <p class="revenue-item-text">of their earnings from tips*</p>
                    </div>
                    <div class="revenue-item">
                        <div class="revenue-item-number">81%</div>
                        <p class="revenue-item-text">of guests didn’t have<br> any cash for tips*</p>
                    </div>
                    <div class="revenue-item">
                        <div class="revenue-item-text-top">cash accounted for only</div>
                        <div class="revenue-item-number">19%</div>
                        <p class="revenue-item-text">of all transactions in 2020**</p>
                    </div>
                </div>
                <p class="revenue-text">*according to transnational company internal research service</p>
                <p class="revenue-text">**according tj the Federal Reserve’s 2021 report </p>
            </div>
        </section>

        <section class="steps">
            <div class="c-container">
                <h2 class="steps-title s-title fadeInUp wow">Get start to work<br> with QRTips — <span>it’s easy!</span>
                </h2>
                <div class="steps-slider swiper">
                    <div class="swiper-wrapper">
                        <div class="steps-item-wrap swiper-slide">
                            <div class="steps-item">
                                <div class="steps-item-img">
                                    <img src="{{ asset('assets/images/steps-1.png') }}" alt="">
                                </div>
                                <p class="steps-item-text">Staff registers<br> in the application</p>
                            </div>
                        </div>
                        <div class="steps-item-wrap swiper-slide">
                            <div class="steps-item">
                                <div class="steps-item-img">
                                    <img src="{{ asset('assets/images/steps-2.png') }}" alt="">
                                </div>
                                <p class="steps-item-text">Gets a personal<br> QR-code or a link</p>
                            </div>
                        </div>
                        <div class="steps-item-wrap swiper-slide">
                            <div class="steps-item">
                                <div class="steps-item-img">
                                    <img src="{{ asset('assets/images/steps-3.png') }}" alt="">
                                </div>
                                <p class="steps-item-text">Guests scans the QR code<br> on the receipt</p>
                            </div>
                        </div>
                        <div class="steps-item-wrap swiper-slide">
                            <div class="steps-item">
                                <div class="steps-item-img">
                                    <img src="{{ asset('assets/images/steps-4.png') }}" alt="">
                                </div>
                                <p class="steps-item-text">Guests choose how much<br> wish to tip and leave a rating
                                </p>
                            </div>
                        </div>
                        <div class="steps-item-wrap swiper-slide">
                            <div class="steps-item">
                                <div class="steps-item-img">
                                    <img src="{{ asset('assets/images/steps-5.png') }}" alt="">
                                </div>
                                <p class="steps-item-text">Tips is instantly wired<br> to its recipient</p>
                            </div>
                        </div>
                    </div>
                    <div class="steps-progress"></div>
                </div>
            </div>
        </section>

    </div>

    <section class="advantages">
        <div class="c-container">
            <div class="advantages-wrapper">
                <div class="advantages-img-wrapper">
                    <div class="advantages-img fadeInLeft wow">
                        <img class="advantages-img-desctop" src="{{ asset('assets/images/advantages-img.png') }}"
                            alt="">
                        <img class="advantages-img-tablet" src="{{ asset('assets/images/advantages-img-tablet.png') }}"
                            alt="">
                        <div class="advantages-tooltip advantages-tooltip-1">Excellent service!</div>
                        <div class="advantages-tooltip advantages-tooltip-2">The best waiter ever!</div>
                        <div class="advantages-tooltip advantages-tooltip-3">Thanks for your service</div>
                    </div>
                </div>
                <div class="advantages-info">
                    <h2 class="advantages-title s-title fadeInUp wow">Get cashless tips<br> with QRTips!</h2>
                    <p class="advantages-desc">If guests don’t have any cash, receive tips at your card or through
                        payment systems using a QR-code</p>
                    <ul class="advantages-list">
                        <li><span><img src="{{ asset('assets/images/advantages-icon-1.svg') }}" alt=""></span>
                            Tips are not taxed</li>
                        <li><span><img src="{{ asset('assets/images/advantages-icon-2.svg') }}" alt=""></span>
                            Easy to transfer money
                            to your card</li>
                        <li><span><img src="{{ asset('assets/images/advantages-icon-3.svg') }}" alt=""></span>
                            Can set up a personal
                            payment page</li>
                    </ul>
                </div>
            </div>
        </div>
        <img class="advantages-shadow" src="{{ asset('assets/images/advantages-shadow.png') }}" alt="">
        <img class="advantages-shadow-tablet" src="{{ asset('assets/images/advantages-shadow-tablet.png') }}"
            alt="">
        <img class="advantages-shadow-mob" src="{{ asset('assets/images/advantages-shadow-mob.png') }}" alt="">
    </section>

    <section class="innovative">
        <div class="c-container">
            <div class="innovative-wrapper">
                <div class="innovative-info">
                    <h2 class="innovative-title s-title fadeInUp wow">QRTips — an innovative business solution!</h2>
                    <p class="innovative-desc">QRTips is boosting employee morale and improving service quality</p>
                    <ul class="innovative-list">
                        <li>Employees have a way to increase income</li>
                        <li>Monitor staff perfomance</li>
                        <li>It works with POS-system R-keeper</li>
                    </ul>
                </div>
                <div class="innovative-img fadeInRight wow">
                    <img src="{{ asset('assets/images/innovative-img.png') }}" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="how">
        <div class="c-container">
            <div class="how-wrapper">
                <div class="how-info">
                    <h2 class="how-title s-title fadeInUp wow">How to tip with QRTips</h2>
                    <p class="how-text">Guests and clients tip for QR-code with <br>their debit/credit cords or using
                        digital options.</p>
                    <p class="how-text">A smartphone is enough to pay, no need to install applications.</p>
                </div>
                <div class="how-img fadeInRight wow">
                    <img src="{{ asset('assets/images/how-img.png') }}" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="comfortable">
        <div class="c-container">
            <h2 class="comfortable-title s-title fadeInUp wow">Tipping with QRTips is convenient!</h2>
            <div class="comfortable-items">
                <div class="comfortable-items-wrap">
                    <div class="comfortable-item">
                        <h3 class="comfortable-item-title">Tipping by QR-code</h3>
                        <p class="comfortable-item-desc">Place the QR-code on POS-materials<br> and any visible
                            surface:</p>
                        <ul class="comfortable-item-list">
                            <li>business cards</li>
                            <li>stickers</li>
                            <li>tent cards</li>
                            <li>checks/bills</li>
                        </ul>
                        <img class="comfortable-item-img-1" src="{{ asset('assets/images/comfortable-img-1.png') }}"
                            alt="">
                    </div>
                    <div class="comfortable-item comfortable-item-2">
                        <h3 class="comfortable-item-title">Link tipping and donations</h3>
                        <p class="comfortable-item-desc">Publish the link on any convenient platform:</p>
                        <ul class="comfortable-item-list">
                            <li>in social networks</li>
                            <li>in messengers</li>
                            <li>on the site</li>
                            <li>on the mailling list</li>
                            <li>in SMS</li>
                        </ul>
                        <img class="comfortable-item-img-2" src="{{ asset('assets/images/comfortable-img-2.png') }}"
                            alt="">
                    </div>
                </div>
                <div class="comfortable-item comfortable-item-big">
                    <h3 class="comfortable-item-title">Integration with cash register programs</h3>
                    <p class="comfortable-item-text">Our service is easy to integrate with professional cash desk
                        applications. Developed modules for systems R-keeper and IIKO prints a-QR code on check.</p>
                    <p class="comfortable-item-text">Our employees will set up integration with your cash register
                        program. Leave us a connection request.</p>
                    <img class="comfortable-item-img-3" src="{{ asset('assets/images/comfortable-img-3.png') }}"
                        alt="">
                </div>
            </div>
        </div>
    </section>

    <footer class="footer">
        <div class="c-container">
            <div class="request">
                <h2 class="request-title s-title fadeInUp wow">Leave a request<br> for connection</h2>
                <form class="request-form">
                    <div class="request-form-input-item">
                        <div class="request-form-input-title">Your name:</div>
                        <input class="request-form-input" type="text" name="name"
                            placeholder="Enter your name" required>
                    </div>
                    <div class="request-form-input-item">
                        <div class="request-form-input-title">Your phone number:</div>
                        <input class="request-form-input phone-input" type="tel" name="phone"
                            placeholder="+ 333 (_ _) - _ _ _ - _ _ - _ _" required>
                    </div>
                    <button class="request-form-btn" type="submit">Send</button>
                </form>
            </div>
            <div class="footer-wrapper">
                <div class="footer-logo">
                    <img src="{{ asset('assets/images/logo.png') }}" alt="">
                </div>
                <div class="footer-contacts">
                    <a href="tel:" class="footer-phone"><img src="{{ asset('assets/images/phone.svg') }}"
                            alt=""> +333 33 333 33
                        33</a>
                    <a href="mailto:qrtips@gmail.com" class="footer-email"><img
                            src="{{ asset('assets/images/email.svg') }}" alt="">
                        qrtips@gmail.com</a>
                </div>
            </div>
            <div class="footer-bottom">
                <p class="footer-copy">Copyright (c) 2022 QRTips</p>
                <a href="#" class="footer-link">Terms and conditions</a>
            </div>
        </div>
        <img class="footer-shadow" src="{{ asset('assets/images/footer-shadow.png') }}" alt="">
        <img class="footer-shadow-mob" src="{{ asset('assets/images/footer-shadow-mob.png') }}" alt="">
    </footer>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{ asset('assets/libs/swiper/swiper-bundle.min.js') }}"></script>
    <script src="{{ asset('assets/libs/wow.min.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
</body>

</html>
