<?php

namespace Modules\Employee\Entities;

use App\Models\ReviewBase;
use Illuminate\Support\Facades\DB;

class Review extends ReviewBase
{
    public function getReviewsArray($userId){
        return DB::table('reviews')
            ->where('user_id', $userId)
            ->orderByDesc('id')
            ->limit(10)
            ->get();
    }

    public function getTotalReviews($userId){
        return DB::table('reviews')
            ->where('user_id', $userId)
            ->count();
    }

    public function getTotalRating($userId){
        return round((DB::table('reviews')
            ->where('user_id', $userId)
            ->average('rating')), 1);
    }

    public function getWeeklyReviews($userId){
        return DB::table('reviews')
            ->where('user_id', $userId)
            ->count();
    }

    public function getWeeklyRating($userId){
        return DB::table('reviews')
            ->where('user_id', $userId)
            ->count();
    }
}
