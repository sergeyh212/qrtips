<?php

namespace Modules\Employee\Entities;

use App\Models\CardBase;
use Illuminate\Support\Facades\DB;

class Card extends CardBase
{
    public function getUserCardsCount($userId){
        return DB::table('cards')
            ->where('user_id', $userId)
            ->count();
    }

    public function getUserCards($userId){
        return DB::table('cards')
            ->select('id', 'mask', 'expiration')
            ->where('user_id', $userId)
            ->get();
    }
}
