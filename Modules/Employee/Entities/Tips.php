<?php

namespace Modules\Employee\Entities;

use App\Models\TipsBase;
use Illuminate\Support\Facades\DB;

class Tips extends TipsBase
{
    public function getTipsArray($userId){
        return DB::table('tips')
            ->where('user_id', auth()->user()->id)
            ->limit(10)
            ->get();
    }

    public function getTotalBalance($userId){
        return DB::table('tips')
            ->where('user_id', $userId)
            ->sum('amount');
    }

    public function getWeeklyAmount($userId){
        return DB::table('tips')
            ->where('user_id', $userId)
            ->where('time', '>', date("Y-m-d H:i:s", strtotime('monday this week')))
            ->sum('amount');
    }

    public static function getUserForWeek($userId){
        $tips = DB::table('tips')
            ->where('user_id', $userId)
            ->where('time', '>', date("Y-m-d H:i:s", strtotime('monday this week')))
            ->get();

        if(!$tips){
            return [];
        }

        $sumTipsDay = [];

        foreach ($tips as $tip) {
            $day = date("D", strtotime($tip->time));

            if(!isset($sumTipsDay[$day])){
                $sumTipsDay[$day] = 0;
            }

            $sumTipsDay[$day] += $tip->amount;
        }

        $days = ['Mon' => 0, 'Tue' => 1, 'Wed' => 2, 'Thu' => 3, 'Fri' => 4, 'Sat' => 5, 'Sun' => 6];

        $tipsSortedByDay = [];

        foreach ($days as $day => $key){

            if(!isset($sumTipsDay[$day])){
                $tipsSortedByDay[$key] = 0;
            }else{
                $tipsSortedByDay[$key] = $sumTipsDay[$day];
            }
        }

        return $tipsSortedByDay;

    }
}
