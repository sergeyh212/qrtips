@extends("employee::layouts.employee.app")
@section("style")
    <link href="assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet"/>
@endsection

@section("wrapper")
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <div class="page-wrapper">
        <div class="page-content">
            <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
                <div class="col">
                    <div class="card radius-10 border-start border-0 border-3 border-danger border-bottom">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-secondary">Your balance</p>
                                    <h4 class="my-1 text-danger">${{$totalBalance}}</h4>
                                    <p class="mb-0 font-13">+ ${{$balanceFromWeek}} from week</p>
                                </div>
                                <div class="widgets-icons-2 rounded-circle bg-gradient-bloody text-white ms-auto"><i class='bx bxs-coin'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card radius-10 border-start border-0 border-3 border-success border-bottom">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-secondary">Money available</p>
                                    <h4 class="my-1 text-success">${{auth()->user()->money}}
                                        @if($countUserCards > 0 && auth()->user()->money > 0)
                                        <a href="{{route('tips.withdrawTipsForm')}}" class="btn-success px-2 btn-sm">Pay money</a></h4>
                                        @elseif($countUserCards > 0 && auth()->user()->money == 0)
                                        <a class="btn-success px-2 btn-sm">Not enough money</a></h4>
                                        @else
                                        <a href="{{route('card.addCard')}}" class="btn-success px-2 btn-sm">Add card</a></h4>
                                        @endif
                                    </h4>
                                    <p class="mb-0 font-13">You can pay money once a day</p>
                                </div>
                                <div class="widgets-icons-2 rounded-circle bg-gradient-ohhappiness text-white ms-auto"><i class='bx bxs-wallet' ></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card radius-10 border-start border-0 border-3 border-info border-bottom">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-secondary">Total reviews</p>
                                    <h4 class="my-1 text-info">{{$totalReviews}}</h4>
                                    <p class="mb-0 font-13">+ {{$reviewsFromWeek}} from week</p>
                                </div>
                                <div class="widgets-icons-2 rounded-circle bg-gradient-scooter text-white ms-auto"><i class='bx bxs-receipt'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card radius-10 border-start border-0 border-3 border-warning border-bottom">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-secondary">Total rating</p>
                                    <h4 class="my-1 text-warning">{{$totalRating}}</h4>
                                    <p class="mb-0 font-13">{{$ratingFromWeek - $totalRating}} ratings from week</p>
                                </div>
                                <div class="widgets-icons-2 rounded-circle bg-gradient-blooker text-white ms-auto"><i class='bx bxs-star'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--end row-->
            <div class="row row-cols-1 row-cols-lg-2">
                <div class="col d-flex">
                    <div class="card radius-10 w-100 border-0 border-3 border-info border-bottom">
                        <div class="card-body">
                            <p class="font-weight-bold mb-1 text-secondary">Weekly tips</p>
                            <div class="d-flex align-items-center mb-4">
                                <div>
                                    <h4 class="mb-0">{{$balanceFromWeek}}</h4>
                                </div>
                                <div class="">
                                </div>
                            </div>
                            <div class="chart-container-0">
                                <canvas id="weeklyTips"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col d-flex">
                    <div class="card radius-10 w-100 border-0 border-3 border-info border-bottom">
                        <div class="card-body">
                            <p class="font-weight-bold mb-1 text-secondary">Weekly reviews</p>
                            <div class="d-flex align-items-center mb-4">
                                <div>
                                    <h4 class="mb-0">{{$reviewsFromWeek}}</h4>
                                </div>
                                <div class=""></div>
                            </div>
                            <div class="chart-container-0">
                                <canvas id="weeklyReviews"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--end row-->
            <div class="row row-cols-1 row-cols-lg-2">
                <div class="col">
                    <div class="card border-0 border-3 border-info border-bottom">
                        <div class="card-body">
                            <div class="card-title">
                                <h5 class="mb-0">Last tips</h5>
                            </div>
                            <hr>

                            @if(count($tipsArray) != 0)
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Tips id</th>
                                            <th>Time</th>
                                            <th>Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($tipsArray as $tips)
                                        <tr>
                                            <td>{{$tips->id}}</td>
                                            <td>{{$tips->time}}</td>
                                            <td>{{$tips->amount}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="{{route('employee.balance')}}" >View all tips</a>
                                </div>
                            @endif

                            @if(count($tipsArray) == 0)
                                <div class="d-flex align-items-center theme-icons p-2 text-center ">
                                    <div class="font-22 text-primary text-center ">
                                        <i class="fadeIn animated bx bx-tired text-center"></i>
                                    </div>
                                    <div class="ms-2">You don't have a tips</div>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card border-0 border-3 border-info border-bottom">
                        <div class="card-body">
                            <div class="card-title">
                                <h5 class="mb-0">Last reviews</h5>
                            </div>
                            <hr>
                            @if(count($reviewsArray) != 0)
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Review id</th>
                                            <th>Time</th>
                                            <th>Rating</th>
                                            <th>Review</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($reviewsArray as $review)
                                            <tr>
                                                <td>{{$review->id}}</td>
                                                <td>{{$review->time}}</td>
                                                <td> @for($i = 0; $i < $review->rating; $i++){{'★'}} @endfor </td>
                                                <td>{{$review->review}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="{{route('employee.reviews')}}" >View all reviews</a>
                                </div>
                            @endif

                            @if(count($reviewsArray) == 0)
                                <div class="d-flex align-items-center theme-icons p-2 text-center ">
                                    <div class="font-22 text-primary text-center ">
                                        <i class="fadeIn animated bx bx-tired text-center"></i>
                                    </div>
                                    <div class="ms-2">You don't have a reviews</div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="assets/plugins/chartjs/js/Chart.min.js"></script>
    <script src="assets/plugins/chartjs/js/Chart.extension.js"></script>
    <script src="assets/plugins/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="assets/js/index.js"></script>
@endsection
