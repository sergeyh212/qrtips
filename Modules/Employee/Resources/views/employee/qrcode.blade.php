@extends("employee::layouts.employee.app")
@section("wrapper")
    <div class="page-wrapper">
        <div class="page-content">
            <div class="container">
                <div class="main-body">
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="col">
                                <div class="card">
                                    <div class="row">
                                        <div class="col-md-4 text-center">
                                            {{ QrCode::size(200)->generate('127.0.0.1/send-tips?id=' . auth()->user()->id)}}
                                        </div>
                                        <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title">QR-code</h5>
                                                <label class="form-label">Main link to get money:</label>
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="button-addon2" value="{{ url('/send-tips?id=' . auth()->user()->id) }}">
                                                    <button type="button" class="btn btn-primary"><i class="bx bx-copy me-0"></i></button>
                                                </div>
                                                <a href="">Download QR-code</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="col">
                                <div class="card">
                                    <div class="row">
                                        <div class="col-md-4 text-center">
                                            {{ QrCode::size(200)->generate('127.0.0.1/send-tips?id=' . auth()->user()->id)}}
                                        </div>
                                        <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title">Business card</h5>
                                                <div class="input-group mb-3">

                                                </div>
                                                <a href="">Download Business card</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



