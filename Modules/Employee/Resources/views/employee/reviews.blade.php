@extends("employee::layouts.employee.app")

@section("style")
    <link href="/assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet"/>
@endsection

@section("wrapper")
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <div class="card border-0 border-3 border-info border-bottom  border-start">
                <div class="card-body">
                    <div class="card-title">
                        <h5 class="mb-0">@lang('main.reviews')</h5>
                    </div>
                    <hr>
                    @if(count($reviewsArray) != 0)
                    <div class="col">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Review Id</th>
                                    <th>@lang('main.time')</th>
                                    <th>@lang('main.review')</th>
                                    <th>@lang('main.rating')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($reviewsArray as $review)
                                    <tr>
                                        <td>{{ $review->id }}</td>
                                        <td>{{ $review->time }}</td>
                                        <td> {{ $review->review }}</td>
                                        <td> @for($i = 0; $i < $review->rating; $i++){{'★'}} @endfor </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                    @if(count($reviewsArray) == 0)
                        <div class="d-flex align-items-center theme-icons p-2 text-center ">
                            <div class="font-22 text-primary text-center ">
                                <i class="fadeIn animated bx bx-tired text-center"></i>
                            </div>
                            <div class="ms-2">You don't have reviews</div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!--end page wrapper -->
@endsection
