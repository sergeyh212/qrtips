@extends("employee::layouts.employee.app")

@section("style")
    <link href="/assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet"/>
@endsection

@section("wrapper")
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <div class="row row-cols-1 row-cols-lg-1">
                <div class="col">
                    <div class="card border-0 border-3 border-info border-bottom  border-start">
                        <div class="card-body">
                            <div class="card-title">
                                <h5 class="mb-0">Payments</h5>
                            </div>
                            <hr>

                            @if(count($paymentsArray) != 0)
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        @foreach($paymentsArray as $payment)
                                            <tr>
                                                <th>Id</th>
                                                <th>Time</th>
                                                <th>Status</th>
                                                <th>Bank card</th>
                                                <th>Amount</th>
                                                <th>Commission</th>
                                            </tr>
                                        @endforeach
                                        </thead>
                                        <tbody>

                                        <tr>
                                            <td>{{$payment->id}}</td>
                                            <td>{{$payment->time}}</td>
                                            @if($payment->status == 0)
                                                <td><span class="badge bg-danger">canceled</span></td>
                                            @elseif($payment->status == 1)
                                                <td><span class="badge bg-success">paid</span></td>
                                            @elseif($payment->status == 2)
                                                <td><span class="badge bg-warning">processing</span></td>
                                            @else
                                                <td><span class="badge bg-secondary">ordered</span></td>
                                            @endif
                                            <td>5356****7854</td>
                                            <td>{{$payment->amount}}</td>
                                            <td>35</td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <nav aria-label="...">
                                        <ul class="pagination">
                                            <li class="page-item disabled"><a class="page-link" href="javascript:;"
                                                                              tabindex="-1" aria-disabled="true">Previous</a>
                                            </li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">1</a>
                                            </li>
                                            <li class="page-item active" aria-current="page"><a class="page-link"
                                                                                                href="javascript:;">2
                                                    <span class="visually-hidden">(current)</span></a>
                                            </li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">3</a>
                                            </li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">Next</a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            @endif

                            @if(count($paymentsArray) == 0)
                                <div class="d-flex align-items-center theme-icons p-2 text-center ">
                                    <div class="font-22 text-primary text-center ">
                                        <i class="fadeIn animated bx bx-tired text-center"></i>
                                    </div>
                                    <div class="ms-2">You don't have payments</div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <!--end page wrapper -->
@endsection
