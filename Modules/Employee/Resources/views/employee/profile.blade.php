@extends("employee::layouts.employee.app")
@section("wrapper")
    <div class="page-wrapper">
        <div class="page-content">
            <form action="{{route('employee.edit')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="container">
                    <div class="main-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="d-flex flex-column align-items-center text-center">
                                            <img src="/assets/images/avatars/avatar-1.png" alt="Admin"
                                                 class="rounded-circle p-1 bg-primary" width="110">
                                            <div class="mt-3">
                                                <button type="button" class="btn btn-primary btn-sm">Upload image</button>
                                            </div>
                                            <div class="mt-3">
                                                <h4>{{$user->first_name . ' ' . $user->last_name}}</h4>
                                            </div>
                                        </div>
                                        <hr class="my-4">
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h5 class="mb-0">Bank cards</h5>

                                        </div>
                                        <hr>
                                        <div class="d-flex flex-column align-items-center text-center">
                                            <table class="table">
                                                <tbody>
                                                @if(count($userCards) != 0)
                                                    @foreach($userCards as $card)
                                                        <tr>
                                                            <td>
                                                                <i class="fadeIn animated bx bx-credit-card"></i>
                                                                {{ $card->mask }}
                                                            </td>
                                                            <td> {{ $card->expiration }} </td>
                                                            <td>
                                                                <a href="{{ route('card.deleteCard', ['id' => $card->id]) }}">
                                                                    <i class="bx bxs-trash me-0"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button type="button" class="btn btn-sm btn-primary px-5"><a href="{{route("card.addCardForm")}}" class="text-white">Add card</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h5 class="mb-0">About me</h5>
                                        </div>
                                        <hr>
                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">First Name</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" class="form-control" name="first_name"
                                                       value="{{$user->first_name}}">
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Last Name</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" class="form-control" name="last_name"
                                                       value="{{$user->last_name}}">
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Phone</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" class="form-control" name="phone"
                                                       value="{{$user->phone}}">
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">@lang('main.country')</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <select class="form-select" name="country_id"
                                                        aria-label="Default select example" name="country_id">
                                                    @foreach($countries as $country)
                                                        @if($country->id === $user->country->id)
                                                            <option selected
                                                                    value="{{ $user->country->id }}">{{ $user->country->name }}</option>
                                                        @else
                                                            <option
                                                                value="{{ $country->id }}">{{ trans('main.' . $country->name) }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Сommission</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <span class="badge bg-primary"><b>{{ auth()->user()->commission }}%</b></span>  <a class="h6 btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModal" HREF="">Do you want to change the commission?</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="submit" class="btn btn-primary px-4"
                                                       value="@lang('main.save_changes')">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection



