<?php

namespace Modules\Employee\Http\Controllers;

use Illuminate\Http\Request;

class QrCodeController extends Controller
{
    public function generateQrCode()
    {
        return view('employee::employee.qrcode');
    }
}
