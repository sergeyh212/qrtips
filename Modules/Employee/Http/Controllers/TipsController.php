<?php

namespace Modules\Employee\Http\Controllers;

use Modules\Employee\Entities\CashFlow;
use Modules\Employee\Entities\Country;
use Modules\Employee\Entities\Payment;
use Modules\Employee\Entities\Review;
use Modules\Employee\Entities\Tips;
use Modules\Employee\Entities\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TipsController extends Controller
{
    public function sendTipsForm(Request $request){
        $userId = $request->id;
        return view('employee::employee.send-tips', compact('userId'));
    }

    public function sendTips(Request $request){
        if(!$request->id){
            return redirect()->route('qrcode.generate');
        }

        $request->validate([
            'name' => 'alpha',
            'tips_count' => 'required|numeric',
        ]);

        $tips = Tips::create([
            'user_id' => $request->id,
            'amount' => $request->tips_count - ($request->tips_count * auth()->user()->commission / 100),
            'currency_code' => 'USD',
            'type' => 1,
            'status' => 0,
            'method' => 1,
            'time' => date("Y-m-d H:i:s"),
            'commission' => ($request->tips_count * auth()->user()->commission) / 100
        ]);

        CashFlow::create([
            'user_id' => $request->id,
            'direction' => 1,
            'amount_before' => DB::table('users')->where('id', $request->id)->value('money'),
            'amount_after' => DB::table('users')->where('id', $request->id)->value('money') + $request->tips_count,
            'amount' => $request->tips_count,
            'currency_code' => 'USD',
            'currency_rate' => 1,
            'oper_id' => $tips->id,
        ]);

        if(isset($request->reviewer_name) && isset($request->review) && isset($request->rating)){
            Review::create([
                'user_id' => $request->id,
                'reviewer_name' => $request->reviewer_name,
                'review' => $request->review,
                'rating' => $request->rating,
                'time' => date("Y-m-d H:i:s")
            ]);
        }

        DB::table('users')->increment('money', $request->tips_count - $request->tips_count * auth()->user()->commission / 100 );

        return redirect()->route('qrcode.generate');
    }

    public function withdrawTipsForm(Request $request){
        return view('employee::employee.withdraw-tips');
    }

    public function withdrawTips(Request $request){
        $oper_id = rand(1, 10000);
        Payment::create([
            'id' => $oper_id,
            'user_id' => auth()->user()->id,
            'amount' => $request->tips_count,
            'currency_code' => 'USD',
            'type' => 1,
            'status' => Payment::S_CONSIDERATION,
            'method' => 1,
            'time' => date("Y-m-d H:i:s")
        ]);

        CashFlow::create([
            'user_id' => auth()->user()->id,
            'direction' => 0,
            'amount_before' => DB::table('users')->where('id', auth()->user()->id)->value('money'),
            'amount_after' => DB::table('users')->where('id', auth()->user()->id)->value('money') - $request->tips_count,
            'amount' => $request->tips_count,
            'currency_code' => 'USD',
            'currency_rate' => 1,
            'oper_id' => $oper_id,
        ]);

        DB::table('users')->decrement('money', $request->tips_count);

        return redirect()->route('employee.index');
    }
}
