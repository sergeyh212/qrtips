<?php

namespace Modules\Employee\Http\Controllers;

use Modules\Employee\Entities\Card;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;


class CardController extends Controller
{
    public function addCardForm(){
        return view('employee::employee.add-card');
    }

    public function addCard(Request $request){
        $mask = $request->card_number;
        $mask = substr_replace(substr_replace($mask, '****',5,4), '****',10,4);
        Card::create([
            'user_id' => auth()->user()->id,
            'name' => $request->name,
            'number' => $request->card_number,
            'expiration' => $request->expiration_date,
            'code' => $request->security_code,
            'mask' => $mask
        ]);
        return redirect()->route('employee.index');
    }

    public function deleteCard(Request $request){
        Card::where("id" , $request->id)->delete();
        return redirect()->back();
    }
}
