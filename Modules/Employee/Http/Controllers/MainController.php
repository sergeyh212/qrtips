<?php

namespace Modules\Employee\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Modules\Employee\Entities\Tips;

class MainController extends Controller
{
    public function changeLocale($locale){
        session(['locale' => $locale]);
        App::setLocale($locale);
        return redirect()->back();
    }

    public function getUserTipsForWeekJson(){

        return response()->json([
            'data' => Tips::getUserForWeek(auth()->user()->id)
        ]);
    }
}
