<?php

use Illuminate\Support\Facades\Route;
use Modules\Admin\Http\Controllers\AdminController;

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function (){
    Route::get('/', [AdminController::class, 'index'])->name('admin.index');
    Route::get('/users', [AdminController::class, 'users'])->name('admin.users');
    Route::get('/reviews', [AdminController::class, 'reviews'])->name('admin.reviews');
    Route::get('/user', [AdminController::class, 'userCard'])->name('admin.userCard');
    Route::post('/user', [AdminController::class, 'userEdit'])->name('admin.userEdit');
    Route::get('/tips', [AdminController::class, 'tips'])->name('admin.tips');
    Route::get('/payments', [AdminController::class, 'payments'])->name('admin.payments');
    Route::get('/review-delete', [AdminController::class, 'reviewDelete'])->name('admin.reviewDelete');
    Route::get('/payment/toApprove', [AdminController::class, 'paymentApprove'])->name('admin.paymentApprove');
    Route::get('/payment/toCancel', [AdminController::class, 'paymentCancel'])->name('admin.paymentCancel');
});
