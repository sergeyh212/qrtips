<?php

namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\Country;
use Modules\Admin\Entities\Payment;
use Modules\Admin\Entities\Review;
use Modules\Admin\Entities\Tips;
use Modules\Admin\Entities\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class AdminController extends \App\Http\Controllers\Controller
{
    public function index(){
        $Tips = new Tips();

        $commissionFromWeek = $Tips->getWeeklyCommission();
        $commissionFromDay = $Tips->getDailyCommission();
        $tipsFromWeek = $Tips->getWeeklyTips();
        $tipsFromDay = $Tips->getDailyTips();
        $lastTips = $Tips->getLastTips();


        $User = new User();

        $usersBalance = $User->getUsersBalance();
        $lastUsers = $User->getLastUsers();


        $Payment = new Payment();

        $moneyOrdered = $Payment->getOrderedMoney();
        $moneyPaided = $Payment->getPaidedMoney();
        $moneyOrderedFromDay = $Payment->getDailyOrderedMoney();
        $lastPayments = $Payment->getLastPayments();


        $Review = new Review();
        $totalReviews = $Review->getTotalReviews();
        $lastReviews = $Review->getLastReviews();

        return view('admin::admin.index', compact('commissionFromWeek', 'commissionFromDay', 'tipsFromWeek', 'tipsFromDay', 'usersBalance', 'moneyOrdered', 'moneyOrderedFromDay', 'moneyPaided', 'totalReviews', 'lastTips', 'lastUsers', 'lastPayments', 'lastReviews'));
    }

    public function users(){
        $usersArray = DB::table('users')->orderByDesc('id')->get();
        return view('admin::admin.users', compact('usersArray'));
    }

    public function tips(){
        $tipsArray = DB::table('tips')
            ->join('users', 'tips.user_id', '=', 'users.id')
            ->select('tips.*', 'users.first_name', 'users.last_name')
            ->orderByDesc('tips.id')
            ->get();
        return view('admin::admin.tips', compact('tipsArray'));
    }

    public function payments(){
        $paymentsArray = DB::table('payments')
            ->join('users', 'payments.user_id', '=', 'users.id')
            ->select('payments.*', 'users.first_name', 'users.last_name')
            ->orderByDesc('payments.id')
            ->get();
        return view('admin::admin.payments', compact('paymentsArray'));
    }

    public function reviews(){
        $reviewsArray = DB::table('reviews')
            ->join('users', 'reviews.user_id', '=', 'users.id')
            ->select('reviews.*', 'users.first_name', 'users.last_name')
            ->orderByDesc('reviews.id')
            ->get();
        return view('admin::admin.reviews', compact('reviewsArray'));
    }

    public function userCard(Request $request){
        if(!$request->user){
            return redirect()->back();
        }
        $countries = Country::all();

        $user = User::find($request->user);

        $userId = $request->user;

        $paymentsArray = User::find($request->user)->payments;
        $tipsArray = User::find($request->user)->tips;
        $reviewsArray = User::find($request->user)->reviews;

        return view('admin::admin.userCard', compact('user', 'countries', 'userId', 'paymentsArray', 'tipsArray', 'reviewsArray'));
    }

    public function userEdit(Request $request){
//        TODO добавить валидацию
        User::where("id",$request->user)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'country_id' => $request->country_id,
            'phone' => $request->phone,
            'email' => $request->email,
            'money' => $request->money
        ]);

        return redirect()->back();
    }

    public function reviewDelete(Request $request){
        Review::where("id" , $request->id)->delete();

        return redirect()->back();
    }

    public function paymentApprove(Request $request){
        Payment::where("id", $request->payment)->update([
           'status' => Payment::S_APPROVE,
        ]);

        return redirect()->back();
    }

    public function paymentCancel(Request $request){
        Payment::where("id", $request->payment)->update([
            'status' => Payment::S_CANCELED,
        ]);

        return redirect()->back();
    }

}

