<?php

namespace Modules\Admin\Entities;

use App\Models\TipsBase;
use Illuminate\Support\Facades\DB;

class Tips extends TipsBase
{

    public function getWeeklyCommission(){
        return DB::table('tips')
            ->where('time', '>', date("Y-m-d H:i:s", strtotime('monday this week')))
            ->sum('commission');
    }

    public function getDailyCommission(){
        return DB::table('tips')
            ->whereDate('time', '=' , date("Y-m-d H:i:s", strtotime('today')))
            ->sum('commission');
    }

    public function getWeeklyTips(){
        return DB::table('tips')
            ->where('time', '>', date("Y-m-d H:i:s", strtotime('monday this week')))
            ->sum('amount');
    }

    public function getDailyTips(){
        return DB::table('tips')
            ->whereDate('time', '=' , date("Y-m-d H:i:s", strtotime('today')))
            ->sum('amount');
    }

    public function getLastTips(){
        return DB::table('tips')
            ->join('users', 'tips.user_id', '=', 'users.id')
            ->select('tips.*', 'users.first_name', 'users.last_name')
            ->orderByDesc('tips.id')
            ->limit(10)
            ->get();
    }
}
