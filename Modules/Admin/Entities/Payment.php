<?php

namespace Modules\Admin\Entities;

use App\Models\PaymentBase;
use Illuminate\Support\Facades\DB;

class Payment extends PaymentBase
{
    public function getOrderedMoney(){
        return DB::table('payments')
            ->where('status', Payment::S_CONSIDERATION)
            ->sum('amount');
    }

    public function getPaidedMoney(){
        return DB::table('payments')
            ->where('status', Payment::S_APPROVE)
            ->sum('amount');
    }

    public function getDailyOrderedMoney(){
        return DB::table('payments')
            ->where('status', Payment::S_CONSIDERATION)
            ->whereDate('time', '=' , date("Y-m-d H:i:s", strtotime('today')))
            ->sum('amount');
    }

    public function getLastPayments(){
        return DB::table('payments')
            ->join('users', 'payments.user_id', '=', 'users.id')
            ->select('payments.*', 'users.first_name', 'users.last_name')
            ->orderByDesc('payments.id')
            ->limit(10)
            ->get();
    }
}
