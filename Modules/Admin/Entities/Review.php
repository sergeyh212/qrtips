<?php

namespace Modules\Admin\Entities;

use App\Models\ReviewBase;
use Illuminate\Support\Facades\DB;

class Review extends ReviewBase
{
    public function getTotalReviews(){
        return DB::table('reviews')
            ->count();
    }

    public function getLastReviews(){
        return DB::table('reviews')
            ->join('users', 'reviews.user_id', '=', 'users.id')
            ->select('reviews.*', 'users.first_name', 'users.last_name')
            ->orderByDesc('reviews.id')
            ->limit(10)
            ->get();
    }
}
