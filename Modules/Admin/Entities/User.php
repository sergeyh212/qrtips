<?php

namespace Modules\Admin\Entities;

use App\Models\UserBase;
use Illuminate\Support\Facades\DB;

class User extends UserBase
{
    public function getUsersBalance(){
        return DB::table('users')
            ->sum('money');
    }

    public function getLastUsers(){
        return DB::table('users')
            ->orderByDesc('id')
            ->limit(10)
            ->get();
    }
}
