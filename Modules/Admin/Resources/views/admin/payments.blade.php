@extends("admin::layouts.admin.app")

@section("style")
    <link href="/assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet"/>
@endsection

@section("wrapper")
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <div class="card border-0 border-3 border-info border-bottom  border-start">
                <div class="card-body">
                    <div class="card-title">
                        <h5 class="mb-0">@lang('main.payments')</h5>
                    </div>
                    <hr>
                    @if(count($paymentsArray) != 0)
                        <div class="col">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>@lang('main.user')</th>
                                        <th>@lang('main.amount')</th>
                                        <th>@lang('main.currency')</th>
                                        <th>@lang('main.type')</th>
                                        <th>@lang('main.status')</th>
                                        <th>@lang('main.time')</th>
                                        <th>@lang('main.actions')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($paymentsArray as $payment)
                                        <tr>
                                            <td>{{$payment->id}}</td>
                                            <td>
                                                <a href="{{ route('admin.userCard', ['user' => $payment->user_id]) }}">{{$payment->first_name . ' ' . $payment->last_name}}</a>
                                            </td>
                                            <td>{{$payment->amount}}</td>
                                            <td>{{$payment->currency_code}}</td>
                                            <td>{{$payment->type}}</td>
                                            <td>{{$payment->status}}</td>
                                            <td>{{$payment->time}}</td>
                                            <td>
                                                <div class="col">
                                                    @if($payment->status == 2)
                                                        <a href="{{ route('admin.paymentApprove', ['payment' => $payment->id]) }}">
                                                            <button type="button" class="btn btn-outline-primary"><i
                                                                    class='lni lni-checkmark'></i>
                                                            </button>
                                                        </a>
                                                        <a href="{{ route('admin.paymentCancel', ['payment' => $payment->id]) }}">
                                                            <button type="button" class="btn btn-outline-primary"><i
                                                                    class='lni lni-close'></i>
                                                            </button>
                                                        </a>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endif
                    @if(count($paymentsArray) == 0)
                        <div class="d-flex align-items-center theme-icons p-2 text-center ">
                            <div class="font-22 text-primary text-center ">
                                <i class="fadeIn animated bx bx-tired text-center"></i>
                            </div>
                            <div class="ms-2">No payments</div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!--end page wrapper -->
@endsection
