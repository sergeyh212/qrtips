@extends("admin::layouts.admin.app")
@section("style")
    <link href="assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet"/>
@endsection

@section("wrapper")
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <div class="page-wrapper">
        <div class="page-content">
            <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
                <div class="col">
                    <div class="card radius-10 border-start border-0 border-3 border-danger">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-secondary">Commissions from week</p>
                                    <h4 class="my-1 text-danger">${{ $commissionFromWeek }}</h4>
                                    <p class="mb-0 font-13">+ ${{$commissionFromDay}} from day</p>
                                </div>
                                <div class="widgets-icons-2 rounded-circle bg-gradient-bloody text-white ms-auto"><i
                                        class='bx bxs-coin'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card radius-10 border-start border-0 border-3 border-success">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-secondary">Tips from week</p>
                                    <h4 class="my-1 text-success">${{ $tipsFromWeek }}</h4>
                                    <p class="mb-0 font-13">+ ${{ $tipsFromDay }} from day</p>
                                </div>
                                <div class="widgets-icons-2 rounded-circle bg-gradient-ohhappiness text-white ms-auto">
                                    <i class='bx bxs-wallet'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card radius-10 border-start border-0 border-3 border-warning">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-secondary">Money ordered</p>
                                    <h4 class="my-1 text-warning">${{$moneyOrdered}}</h4>
                                    <p class="mb-0 font-13">+ ${{$moneyOrderedFromDay}} from day</p>
                                </div>
                                <div class="widgets-icons-2 rounded-circle bg-gradient-blooker text-white ms-auto"><i
                                        class='bx bxs-star'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card radius-10 border-start border-0 border-3 border-info">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-secondary">Users balance</p>
                                    <h4 class="my-1 text-info">${{$usersBalance}}</h4>
                                    <p class="mb-0 font-13">+ ${{$tipsFromDay}} from day</p>
                                </div>
                                <div class="widgets-icons-2 rounded-circle bg-gradient-scooter text-white ms-auto"><i
                                        class='bx bxs-user'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--end row-->
            <div class="row row-cols-1 row-cols-lg-2">
                <div class="col d-flex">
                    <div class="card radius-10 w-100">
                        <div class="card-body">
                            <p class="font-weight-bold mb-1 text-secondary">Commissions</p>
                            <div class="d-flex align-items-center mb-4">
                                <div>
                                    <h4 class="mb-0">${{$commissionFromWeek}}</h4>
                                </div>
                                <div class=""></div>
                            </div>
                            <div class="chart-container-0">
                                <canvas id="chart3"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col d-flex">
                    <div class="card radius-10 w-100">
                        <div class="card-body">
                            <p class="font-weight-bold mb-1 text-secondary">Tips</p>
                            <div class="d-flex align-items-center mb-4">
                                <div>
                                    <h4 class="mb-0">${{$tipsFromWeek}}</h4>
                                </div>
                                <div class="">
                                </div>
                            </div>
                            <div class="chart-container-0">
                                <canvas id="chart3"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col d-flex">
                    <div class="card radius-10 w-100">
                        <div class="card-body">
                            <p class="font-weight-bold mb-1 text-secondary">Paid money</p>
                            <div class="d-flex align-items-center mb-4">
                                <div>
                                    <h4 class="mb-0">${{$moneyPaided}}</h4>
                                </div>
                                <div class=""></div>
                            </div>
                            <div class="chart-container-0">
                                <canvas id="chart3"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col d-flex">
                    <div class="card radius-10 w-100">
                        <div class="card-body">
                            <p class="font-weight-bold mb-1 text-secondary">Reviews</p>
                            <div class="d-flex align-items-center mb-4">
                                <div>
                                    <h4 class="mb-0">{{$totalReviews}}</h4>
                                </div>
                                <div class=""></div>
                            </div>
                            <div class="chart-container-0">
                                <canvas id="chart3"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--end row-->
            <div class="row row-cols-1 row-cols-lg-2">
                <div class="col">
                    <div class="card border-0 border-3 border-info border-bottom">
                        <div class="card-body">
                            <div class="card-title">
                                <h5 class="mb-0">Last tips</h5>
                            </div>
                            <hr>
                            @if(count($lastTips) != 0)
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Tip id</th>
                                            <th>Time</th>
                                            <th>User</th>
                                            <th>Amount</th>
                                            <th>Commission</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($lastTips as $tips)
                                            <tr>
                                                <td>{{$tips->id}}</td>
                                                <td>{{$tips->time}}</td>
                                                <td>
                                                    <a href="{{ route('admin.userCard', ['user' => $tips->user_id]) }}">{{$tips->first_name . ' ' . $tips->last_name}}</a>
                                                </td>
                                                <td>{{$tips->amount}}</td>
                                                <td>{{$tips->commission}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="{{route('admin.tips')}}">View all tips</a>
                                </div>
                            @endif
                            @if(count($lastTips) == 0)
                                <div class="d-flex align-items-center theme-icons p-2 text-center ">
                                    <div class="font-22 text-primary text-center ">
                                        <i class="fadeIn animated bx bx-tired text-center"></i>
                                    </div>
                                    <div class="ms-2">No tips</div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card border-0 border-3 border-info border-bottom">
                        <div class="card-body">
                            <div class="card-title">
                                <h5 class="mb-0">Last users</h5>
                            </div>
                            <hr>
                            @if(count($lastUsers) != 0)
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Tip id</th>
                                            <th>Time</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Money</th>
                                            <th>Commission</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($lastUsers as $user)
                                            <tr>
                                                <td>{{$user->id}}</td>
                                                <td>{{$user->created_at}}</td>
                                                <td>
                                                    <a href="{{ route('admin.userCard', ['user' => $user->id]) }}">{{$user->first_name . ' ' . $user->last_name}}</a>
                                                </td>
                                                <td>{{$user->phone}}</td>
                                                <td>{{$user->money}}</td>
                                                <td>{{$user->commission}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="{{route('admin.users')}}">View all users</a>
                                </div>
                            @endif
                            @if(count($lastUsers) == 0)
                                <div class="d-flex align-items-center theme-icons p-2 text-center ">
                                    <div class="font-22 text-primary text-center ">
                                        <i class="fadeIn animated bx bx-tired text-center"></i>
                                    </div>
                                    <div class="ms-2">No users</div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-cols-1 row-cols-lg-2">
                <div class="col">
                    <div class="card border-0 border-3 border-info border-bottom">
                        <div class="card-body">
                            <div class="card-title">
                                <h5 class="mb-0">Last payouts</h5>
                            </div>
                            <hr>
                            @if(count($lastPayments) != 0)
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Tip id</th>
                                            <th>Time</th>
                                            <th>User</th>
                                            <th>Amount</th>
                                            <th>Commission</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($lastPayments as $payment)
                                            <tr>
                                                <td>{{$payment->id}}</td>
                                                <td>{{$payment->time}}</td>
                                                <td>
                                                    <a href="{{ route('admin.userCard', ['user' => $payment->user_id]) }}">{{$payment->first_name . ' ' . $payment->last_name}}</a>
                                                </td>
                                                <td>{{$payment->amount}}</td>
                                                <td>{{123}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="{{route('admin.payments')}}">View all payouts</a>
                                </div>
                            @endif
                            @if(count($lastPayments) == 0)
                                <div class="d-flex align-items-center theme-icons p-2 text-center ">
                                    <div class="font-22 text-primary text-center ">
                                        <i class="fadeIn animated bx bx-tired text-center"></i>
                                    </div>
                                    <div class="ms-2">No payments</div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card border-0 border-3 border-info border-bottom">
                        <div class="card-body">
                            <div class="card-title">
                                <h5 class="mb-0">Last reviews not processed</h5>
                            </div>
                            <hr>
                            @if(count($lastReviews) != 0)
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>To User</th>
                                            <th>Time</th>
                                            <th>Rating</th>
                                            <th>Review</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($lastReviews as $review)
                                            <tr>
                                                <td>{{$review->id}}</td>
                                                <td>
                                                    <a href="{{ route('admin.userCard', ['user' => $review->user_id]) }}">{{$review->first_name . ' ' . $review->last_name}}</a>
                                                </td>
                                                <td>{{$review->time}}</td>
                                                <td> @for($i = 0; $i < $review->rating; $i++){{'★'}} @endfor </td>
                                                <td>{{$review->review}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="{{route('admin.reviews')}}">View all reviews</a>
                                </div>
                            @endif
                            @if(count($lastReviews) == 0)
                                <div class="d-flex align-items-center theme-icons p-2 text-center ">
                                    <div class="font-22 text-primary text-center ">
                                        <i class="fadeIn animated bx bx-tired text-center"></i>
                                    </div>
                                    <div class="ms-2">No reviews</div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <h3>Нижние блоки удали. Я их не трогал. Из-за их удаления падают графики выше. Тебе нужно разобраться
                    как работают графики и как туда попадают данные</h3>
                <div class="col-12 col-lg-8">
                    <div class="card radius-10">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <h6 class="mb-0">Sales Overview</h6>
                                </div>
                                <div class="dropdown ms-auto">
                                    <a class="dropdown-toggle dropdown-toggle-nocaret" href="#"
                                       data-bs-toggle="dropdown"><i
                                            class='bx bx-dots-horizontal-rounded font-22 text-option'></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="javascript:;">Action</a>
                                        </li>
                                        <li><a class="dropdown-item" href="javascript:;">Another action</a>
                                        </li>
                                        <li>
                                            <hr class="dropdown-divider">
                                        </li>
                                        <li><a class="dropdown-item" href="javascript:;">Something else here</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="d-flex align-items-center ms-auto font-13 gap-2 my-3">
                                <span class="border px-1 rounded cursor-pointer"><i class="bx bxs-circle me-1"
                                                                                    style="color: #ffc107"></i>Visits</span>
                            </div>
                            <div class="chart-container-1">
                                <canvas id="chart1"></canvas>
                            </div>
                        </div>
                        <div class="row row-cols-1 row-cols-md-3 row-cols-xl-3 g-0 row-group text-center border-top">
                            <div class="col">
                                <div class="p-3">
                                    <h5 class="mb-0">24.15M</h5>
                                    <small class="mb-0">Overall Visitor <span> <i
                                                class="bx bx-up-arrow-alt align-middle"></i> 2.43%</span></small>
                                </div>
                            </div>
                            <div class="col">
                                <div class="p-3">
                                    <h5 class="mb-0">12:38</h5>
                                    <small class="mb-0">Visitor Duration <span> <i
                                                class="bx bx-up-arrow-alt align-middle"></i> 12.65%</span></small>
                                </div>
                            </div>
                            <div class="col">
                                <div class="p-3">
                                    <h5 class="mb-0">639.82</h5>
                                    <small class="mb-0">Pages/Visit <span> <i
                                                class="bx bx-up-arrow-alt align-middle"></i> 5.62%</span></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card radius-10">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <h6 class="mb-0">Trending Products</h6>
                                </div>
                                <div class="dropdown ms-auto">
                                    <a class="dropdown-toggle dropdown-toggle-nocaret" href="#"
                                       data-bs-toggle="dropdown"><i
                                            class='bx bx-dots-horizontal-rounded font-22 text-option'></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="javascript:;">Action</a>
                                        </li>
                                        <li><a class="dropdown-item" href="javascript:;">Another action</a>
                                        </li>
                                        <li>
                                            <hr class="dropdown-divider">
                                        </li>
                                        <li><a class="dropdown-item" href="javascript:;">Something else here</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="chart-container-2 mt-4">
                                <canvas id="chart2"></canvas>
                            </div>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex bg-transparent justify-content-between align-items-center">
                                Jeans <span class="badge bg-success rounded-pill">25</span>
                            </li>
                            <li class="list-group-item d-flex bg-transparent justify-content-between align-items-center">
                                T-Shirts <span class="badge bg-danger rounded-pill">10</span>
                            </li>
                            <li class="list-group-item d-flex bg-transparent justify-content-between align-items-center">
                                Shoes <span class="badge bg-primary rounded-pill">65</span>
                            </li>
                            <li class="list-group-item d-flex bg-transparent justify-content-between align-items-center">
                                Lingerie <span class="badge bg-warning text-dark rounded-pill">14</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!--end row-->
        </div>
    </div>
@endsection

@section("script")
    <script src="assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="assets/plugins/chartjs/js/Chart.min.js"></script>
    <script src="assets/plugins/chartjs/js/Chart.extension.js"></script>
    <script src="assets/plugins/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="assets/js/index.js"></script>
@endsection
