@extends("admin::layouts.admin.app")
@section("style")
    <link href="/assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet"/>
@endsection
@section("wrapper")
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <div class="card border-0 border-3 border-info border-bottom  border-start">
                <div class="card-body">
                    <div class="card-title">
                        <h5 class="mb-0">@lang('main.tips')</h5>
                    </div>
                    <hr>
                    @if(count($tipsArray) != 0)
                        <div class="col">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>@lang('main.user')</th>
                                        <th>@lang('main.amount')</th>
                                        <th>@lang('main.currency')</th>
                                        <th>@lang('main.type')</th>
                                        <th>@lang('main.status')</th>
                                        <th>@lang('main.time')</th>
                                        <th>@lang('main.commission')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tipsArray as $tips)
                                        <tr>
                                            <td>{{$tips->id}}</td>
                                            <td>
                                                <a href="{{ route('admin.userCard', ['user' => $tips->user_id]) }}">{{$tips->first_name . ' ' . $tips->last_name}}</a>
                                            </td>
                                            <td>{{$tips->amount}}</td>
                                            <td>{{$tips->currency_code}}</td>
                                            <td>{{$tips->type}}</td>
                                            <td>{{$tips->status}}</td>
                                            <td>{{$tips->time}}</td>
                                            <td>{{$tips->commission}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endif
                    @if(count($tipsArray) == 0)
                        <div class="d-flex align-items-center theme-icons p-2 text-center ">
                            <div class="font-22 text-primary text-center ">
                                <i class="fadeIn animated bx bx-tired text-center"></i>
                            </div>
                            <div class="ms-2">No tips</div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!--end page wrapper -->
@endsection
