@extends("admin::layouts.admin.app")
@section("style")
    <link href="/assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet"/>
@endsection
@section("wrapper")
    <div class="page-wrapper">
        <div class="page-content">
            <form method="post" action="{{route('admin.userEdit', ['user' => $userId])}}">
                @csrf
                <div class="container">
                    <div class="main-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">ID</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input readonly type="text" class="form-control" name="first_name"
                                                       value="{{$user->id}}"/>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">@lang('main.first_name')</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" class="form-control" name="first_name"
                                                       value="{{$user->first_name}}"/>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">@lang('main.last_name')</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" class="form-control" name="last_name"
                                                       value="{{$user->last_name}}"/>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">@lang('main.country')</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <select class="form-select" name="country_id"
                                                        aria-label="Default select example">
                                                    <option selected
                                                            value="{{ $user->country->id }}">{{ $user->country->name }}</option>
                                                    @foreach($countries as $country)
                                                        <option
                                                            value="{{ $country->id}}">{{ trans('main.' . $country->name) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">@lang('main.phone')</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" class="form-control" name="phone"
                                                       value="{{$user->phone}}"/>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">@lang('main.email_adress')</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" class="form-control" name="email"
                                                       value="{{$user->email}}"/>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">@lang('main.money')</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="number" name="money" class="form-control"
                                                       value="{{$user->money}}"/>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">@lang('main.reg_at')</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input readonly type="text" class="form-control"
                                                       value="{{$user->created_at}}"/>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">@lang('main.upd_at')</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input readonly type="text" class="form-control"
                                                       value="{{$user->updated_at}}"/>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="submit" class="btn btn-primary px-4"
                                                       value="@lang('main.save_changes')"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div class="card border-0 border-3 border-info border-bottom  border-start">
                <div class="card-title">
                    <h5 class="mb-0 ms-2">@lang('main.tips')</h5>
                </div>
                <hr>
                @if(count($tipsArray) != 0)
                    <div class="col">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>@lang('main.amount')</th>
                                    <th>@lang('main.currency')</th>
                                    <th>@lang('main.type')</th>
                                    <th>@lang('main.status')</th>
                                    <th>@lang('main.time')</th>
                                    <th>@lang('main.commission')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tipsArray as $tips)
                                    <tr>
                                        <td>{{$tips->id}}</td>
                                        <td>{{$tips->amount}}</td>
                                        <td>{{$tips->currency_code}}</td>
                                        <td>{{$tips->type}}</td>
                                        <td>{{$tips->status}}</td>
                                        <td>{{$tips->time}}</td>
                                        <td>{{$tips->commission}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
                @if(count($tipsArray) == 0)
                    <div class="d-flex align-items-center theme-icons p-2 text-center ">
                        <div class="font-22 text-primary text-center ">
                            <i class="fadeIn animated bx bx-tired text-center"></i>
                        </div>
                        <div class="ms-2">User don't have payments</div>
                    </div>
                @endif
            </div>

            <div class="card border-0 border-3 border-info border-bottom  border-start">
                <div class="card-title">
                    <h5 class="mb-0 ms-2">@lang('main.payments')</h5>
                </div>
                <hr>
                @if(count($paymentsArray) != 0)
                    <div class="col">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>@lang('main.amount')</th>
                                    <th>@lang('main.currency')</th>
                                    <th>@lang('main.type')</th>
                                    <th>@lang('main.status')</th>
                                    <th>@lang('main.type')</th>
                                    <th>@lang('main.actions')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($paymentsArray as $payment)
                                    <tr>
                                        <td>{{$payment->id}}</td>
                                        <td>{{$payment->amount}}</td>
                                        <td>{{$payment->currency_code}}</td>
                                        <td>{{$payment->type}}</td>
                                        <td>{{$payment->status}}</td>
                                        <td>{{$payment->time}}</td>
                                        <td>
                                            <div class="col">
                                                @if($payment->status == 2)
                                                    <a href="{{ route('admin.paymentApprove', ['payment' => $payment->id]) }}">
                                                        <button type="button" class="btn btn-outline-primary"><i
                                                                class='lni lni-checkmark'></i>
                                                        </button>
                                                    </a>
                                                    <a href="{{ route('admin.paymentCancel', ['payment' => $payment->id]) }}">
                                                        <button type="button" class="btn btn-outline-primary"><i
                                                                class='lni lni-close'></i>
                                                        </button>
                                                    </a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
                @if(count($tipsArray) == 0)
                    <div class="d-flex align-items-center theme-icons p-2 text-center ">
                        <div class="font-22 text-primary text-center ">
                            <i class="fadeIn animated bx bx-tired text-center"></i>
                        </div>
                        <div class="ms-2">User don't have payments</div>
                    </div>
                @endif
            </div>

            <h5 class="mb-0 ms-2">@lang('main.reviews')</h5>
            <hr/>
            <div class="card border-0 border-3 border-info border-bottom  border-start">
                <div class="row row-cols-1 row-cols-md-2 row-cols-lg-4 row-cols-xl-4">
                    @if (count($reviewsArray))
                        @foreach($reviewsArray as $review)
                            <div class="col">
                                <div class="card border-primary border-bottom border-3 border-0">
                                    <a href="{{ route('admin.reviewDelete', ['id' => $review->id]) }}">
                                        <button type="button" class="btn btn-outline-danger"><i
                                                class='bx bxs-trash-alt me-0 font-20'></i>
                                        </button>
                                    </a>
                                    <div class="card-body">
                                        <h5 class="card-title text-primary"> {{ $review->reviewer_name }}</h5>
                                        <p class="card-text">{{ $review->review }}</p>
                                        @for($i = 0; $i < $review->rating; $i++){{'★'}} @endfor
                                        <hr>
                                        {{ $review->time }}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @elseif(count($tipsArray) == 0)
                        <div class="d-flex align-items-center theme-icons p-2 text-center ">
                            <div class="font-22 text-primary text-center ">
                                <i class="fadeIn animated bx bx-tired text-center ms-3"></i>
                            </div>
                            <div class="ms-2">User don't have reviews</div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection



