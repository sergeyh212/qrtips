@extends("admin::layouts.admin.app")


@section("style")
    <link href="/assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet"/>
@endsection

@section("wrapper")
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <div class="card border-0 border-3 border-info border-bottom  border-start">
                <div class="card-body">
                    <div class="card-title">
                        <h5 class="mb-0">@lang('main.users')</h5>
                    </div>
                    <hr>
                    <div class="col">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>@lang('main.first_name')</th>
                                    <th>@lang('main.last_name')</th>
                                    <th>@lang('main.phone')</th>
                                    <th>@lang('main.email_address')</th>
                                    <th>@lang('main.money')</th>
                                    <th>@lang('main.reg_at')</th>
                                    <th>@lang('main.upd_at')</th>
                                    <th>@lang('main.actions')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($usersArray as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->first_name}}</td>
                                        <td>{{$user->last_name}}</td>
                                        <td>{{$user->phone}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->money}}</td>
                                        <td>{{$user->created_at}}</td>
                                        <td>{{$user->updated_at}}</td>
                                        <td>
                                            <div class="col">
                                                <a href="{{ route('admin.userCard', ['user' => $user->id]) }}">
                                                    <button type="button" class="btn btn-outline-primary"><i
                                                            class='bx bx-user me-0'></i>
                                                    </button>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end page wrapper -->
@endsection
