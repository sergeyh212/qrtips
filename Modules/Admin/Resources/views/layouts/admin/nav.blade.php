<!--sidebar wrapper -->
<div class="sidebar-wrapper" data-simplebar="true">
            <div class="sidebar-header">
                <div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
                </div>
            </div>
            <!--navigation-->
            <ul class="metismenu" id="menu">
                <li>
                    <a href="{{ route('admin.index') }}">
                        <div class="parent-icon"><i class="fadeIn animated bx bx-home-alt"></i>
                        </div>
                        <div class="menu-title">@lang('main.main_page')</div>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.users') }}">
                        <div class="parent-icon"><i class="bx bx-user-circle"></i>
                        </div>
                        <div class="menu-title">@lang('main.users')</div>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.tips') }}">
                        <div class="parent-icon"><i class="lni lni-money-location"></i>
                        </div>
                        <div class="menu-title">@lang('main.tips')</div>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.payments') }}">
                        <div class="parent-icon"><i class="lni lni-money-location"></i>
                        </div>
                        <div class="menu-title">@lang('main.payments')</div>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.reviews') }}">
                        <div class="parent-icon"><i class="fadeIn animated bx bx-message-square-detail"></i>
                        </div>
                        <div class="menu-title">@lang('main.reviews')</div>
                    </a>
                </li>

            </ul>
            <!--end navigation-->
        </div>
        <!--end sidebar wrapper -->
