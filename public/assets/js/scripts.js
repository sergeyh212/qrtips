$(document).ready(function() {

	var stepsSlider = new Swiper('.steps-slider', {
		slidesPerView: 1,
		spaceBetween: 20,
		slideToClickedSlide: true,
		pagination: {
			el: '.steps-progress',
			type: 'progressbar',
		},
		breakpoints: {
			1024: {
				slidesPerView: 5,
			},
		}
	});

	var windowWidth = $(window).width();
	if(windowWidth > 768){
		new WOW().init();
	}

});
