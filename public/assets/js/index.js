$(function() {
    "use strict";

   // weeklyTips

    var weeklyTipsChart = document.getElementById('weeklyTips').getContext('2d');
    var gradientStroke1 = weeklyTipsChart.createLinearGradient(0, 0, 0, 300);
    gradientStroke1.addColorStop(0, '#008cff');
    gradientStroke1.addColorStop(1, 'rgba(22, 195, 233, 0.1)');

    $(function(){
        jQuery.ajax({
            async: true,
            cache: false,
            type: 'POST',
            url: 'employee/getUserTipsForWeekJson',
            data: {"_token": $('meta[name="csrf-token"]').attr('content')},
            success: function (data, textStatus, jqXHR) {
                console.log(data.data);

                var myChart = new Chart(weeklyTipsChart, {
                    type: 'line',
                    data: {
                        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                        datasets: [{
                            label: 'Revenue',
                            data:data.data,
                            pointBorderWidth: 2,
                            pointHoverBackgroundColor: gradientStroke1,
                            backgroundColor: gradientStroke1,
                            borderColor: gradientStroke1,
                            borderWidth: 3
                        }]
                    },
                    options: {
                        maintainAspectRatio: false,
                        legend: {
                            position: 'bottom',
                            display:false
                        },
                        tooltips: {
                            displayColors:false,
                            mode: 'nearest',
                            intersect: false,
                            position: 'nearest',
                            xPadding: 10,
                            yPadding: 10,
                            caretPadding: 10
                        }
                    }
                });

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("error");
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    });
});
